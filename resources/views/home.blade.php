@extends('layout')

@section('class', 'home')

@section('content')
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">
            <img src="img/preload-logo.gif" alt="Zahi Sifek">
            <br>
            <br>
            <p>Chargement en cours. <br> Merci patienter</p><br>
            <img src="img/preloader.gif" alt="">
        </div>
    </div>

    <div class="app-wrapper">

        <div class="logo-wrapper">
            <img src="img/logo.png" alt="" class="logo">
            <img src="img/bg-logo.png" alt="" class="bg-logo">
        </div>

        {{--<button class="action-btn start-btn">
            <span class="pull-right">Participer
            <span class="sub">cliquez ici</span></span>
            <i class="icon-right"></i>
        </button>--}}


        <div class="page-wrapper">

            <!--
                game Page
            =====================-->
            {{--<div class="page game">
                <div class="content">
                    <div class="description">
                        <h2>la grille</h2>
                    </div>

                    <div id="timer">
                        <div class="face">
                            <p id="duration">40</p>
                            <h2>Sec</h2>
                        </div>
                    </div>

                    <div id='theGrid'></div>

                </div>
            </div>

            <div class="word-list">
                <h3>Les mots à deviner</h3>
                <h4>Trouver ces mots dans la grille</h4>
                <div class="word-wrapper">
                    <ul>

                    </ul>
                </div>
            </div>--}}
            <!-- /End game -->

            <!--
                howTo Page
            =====================-->
            <div class="page howto">
                <div class="content">
                    <div class="description">
                        <h2>Comment jouer</h2>
                        <p>Trouvez les 7 mots indiqués dans la liste située à droite de la grille.
                            Vous avez 90 secondes pour les trouver. <br>
                            Pour valider les mots, il suffit de cliquer et sélectionner de la première à la dernière lettre ou l'inverse</p>
                    </div>
                    <img src="img/howto.png" alt="comment jouer?">

                    <button class="action-btn continue-btn">
                        <span class="pull-right">Continuer
                        <span class="sub">cliquez ici</span></span>
                        <i class="icon-right"></i>
                    </button>
                </div>
            </div>
            <!-- /End howTo -->

            <!--
                howTo2 Page
            =====================-->
            <div class="overlay-howto"></div>
            <div class="page howto2">
                <div class="content">
                    <div class="description">
                        <h2>Comment jouer</h2>
                        <p>Trouvez les 7 mots indiqués dans la liste située à droite de la grille.
                            Vous avez 20 secondes pour les trouver. <br>
                            Pour valider les mots, il suffit de cliquer et sélectionner de la première à la dernière lettre ou l'inverse</p>
                    </div>
                    <img src="img/howto.png" alt="comment jouer?">

                    <button class="action-btn quit-btn">
                        <span class="pull-right">Quitter
                        <span class="sub">cliquez ici</span></span>
                        <i class="icon-right"></i>
                    </button>
                </div>
            </div>
            <!-- /End howTo2 -->


            <!--
                Form Page
            ===================== -->
            <div class="page form">
                <div class="content">
                    <form id="signup-form" action="{{ action('AppController@signup') }}" method="post" accept-charset="utf-8">
                        <!-- {!! csrf_field() !!} -->
                        <div class="description">
                            <h2>Inscription</h2>
                            <p>Pour vous inscrire au tirage au sort, veuillez remplir tous les champs du formulaire</p>
                        </div>
                        <div class="row">
                            <div class="form-field">
                                <input type="text" id="name" name="lastname" class="form-input" placeholder="Nom *" required data-message="Ce champ est obligatoire">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-field">
                                <input type="text" id="lastname" name="firstname" class="form-input" placeholder="Prénom *" required data-message="Ce champ est obligatoire">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-field">
                                <select name="monoprix" class="form-input" required data-message="Choisisser un PDV.">
                                    <option value="" disabled selected>Monoprix le plus proche  *</option>
                                    <option value="Aouina">Aouina</option>
                                    <option value="Ariana">Ariana</option>
                                    <option value="Bab khadra">Bab khadra</option>
                                    <option value="Bab Souika">Bab Souika </option>
                                    <option value="Bardo">Bardo </option>
                                    <option value="Beja">Beja</option>
                                    <option value="Beni khalled ">Beni khalled </option>
                                    <option value="Bizerte avenue Habib Bourguiba">Bizerte avenue Habib Bourguiba</option>
                                    <option value="Bizerte rue Ibn Khaldoun">Bizerte rue Ibn Khaldoun </option>
                                    <option value="Bizerte Zarzouna">Bizerte Zarzouna</option>
                                    <option value="Carthage">Carthage</option>
                                    <option value="Daouar Hicher ">Daouar Hicher </option>
                                    <option value="Djerba Houmet Souk El Hara">Djerba Houmet Souk El Hara</option>
                                    <option value="Djerba rue Abdelhamid El Kadhi ">Djerba rue Abdelhamid El Kadhi </option>
                                    <option value="El Manar 1 rue Malaga ">El Manar 1 rue Malaga </option>
                                    <option value="El Manar 2 Colisée Soula">El Manar 2 Colisée Soula</option>
                                    <option value="El Menzah 4 Saadi">El Menzah 4 Saadi</option>
                                    <option value="El Menzah 6">El Menzah 6</option>
                                    <option value="El Moez">El Moez</option>
                                    <option value="El Mourouj 1">El Mourouj 1</option>
                                    <option value="El Mourouj 3">El Mourouj 3</option>
                                    <option value="Ennasr 1">Ennasr 1</option>
                                    <option value="Ennasr 2">Ennasr 2 </option>
                                    <option value="Ezzahra">Ezzahra</option>
                                    <option value="Gammarth">Gammarth</option>
                                    <option value="Jendouba">Jendouba</option>
                                    <option value="Kairouan">Kairouan</option>
                                    <option value="Korba">Korba</option>
                                    <option value="Kram">Kram</option>
                                    <option value="La Marsa">La Marsa </option>
                                    <option value="La Marsa Zéphyr">La Marsa Zéphyr</option>
                                    <option value="Lac">Lac</option>
                                    <option value="Le Kef">Le Kef</option>
                                    <option value="Medenine">Medenine</option>
                                    <option value="Menzal Temime">Menzal Temime </option>
                                    <option value="Menzel Bourguiba">Menzel Bourguiba</option>
                                    <option value="M'hamdia">M'hamdia</option>
                                    <option value="Mini M Agba">Mini M Agba</option>
                                    <option value="Mini M Bellevue">Mini M Bellevue</option>
                                    <option value="Mini M Borj Lozir">Mini M Borj Lozir</option>
                                    <option value="Mini M Boumhel">Mini M Boumhel</option>
                                    <option value="Mini M El Mourouj">Mini M El Mourouj</option>
                                    <option value="Mini M Ettadhamen">Mini M Ettadhamen</option>
                                    <option value="Mini M Ezzouhour">Mini M Ezzouhour </option>
                                    <option value="Mini M Hamm-chatt">Mini M Hamm-chatt</option>
                                    <option value="Mini M Ksar said">Mini M Ksar said</option>
                                    <option value="Mini M Mégrine">Mini M Mégrine </option>
                                    <option value="Mini M Omrane">Mini M Omrane</option>
                                    <option value="Mini M Sliman">Mini M Sliman </option>
                                    <option value="MINI M Soukra">MINI M Soukra </option>
                                    <option value="Moknine">Moknine </option>
                                    <option value="Monastir">Monastir</option>
                                    <option value="Msaken">Msaken</option>
                                    <option value="Nabeul H.Bourguiba">Nabeul H.Bourguiba</option>
                                    <option value="Nabeul Hédi Chaker">Nabeul Hédi Chaker</option>
                                    <option value="Nefza">Nefza</option>
                                    <option value="Sers">Sers</option>
                                    <option value="Sfax 2000">Sfax 2000</option>
                                    <option value="Sfax centre">Sfax centre</option>
                                    <option value="Sfax Chihia">Sfax Chihia</option>
                                    <option value="Sfax El Aîn">Sfax El Aîn</option>
                                    <option value="Siliana">Siliana</option>
                                    <option value="Sousse Bab Gharbi">Sousse Bab Gharbi</option>
                                    <option value="Sousse El Kantaoui">Sousse El Kantaoui</option>
                                    <option value="Sousse route de Monastir">Sousse route de Monastir</option>
                                    <option value="Sousse Séltène">Sousse Séltène </option>
                                    <option value="Tabarka">Tabarka</option>
                                    <option value="Tunis Central park">Tunis Central park</option>
                                    <option value="Tunis Charles de Gaulle">Tunis Charles de Gaulle  </option>
                                    <option value="Tunis Lafayette">Tunis Lafayette  </option>
                                    <option value="Zaouit Kontech">Zaouit Kontech </option>
                                    <option value="Zarzis">Zarzis</option>
                                    <option value="Zéphyr Kids">Zéphyr Kids</option>
                                    <option value="Zéphyr Maison">Zéphyr Maison</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-field">
                                <input type="text" id="card" name="card" class="form-input" placeholder="Num° LACARTE *" required maxlength="13" pattern="^22000[0-9]{8}$" data-message="Num° incorrecte">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-field">
                                <input type="text" id="cin" name="cin" class="form-input" placeholder="CIN *" required  maxlength="8" pattern="^[0-9]{8}$" data-message="CIN incorrecte">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-field">
                                <input type="tel" id="phone" name="phone" class="form-input" placeholder="Téléphone *" required maxlength="8" pattern="^[2549][0-9]{7}$" data-message="Téléphone incorrecte">
                            </div>
                        </div>
                        <div class="row">
                            <p class="info">En cliquant sur "Validez", vous acceptez le <a href="reglement.pdf" target="_blank">règlement du jeu</a></p>

                            <button type="submit" class="action-btn submit-btn">
                                <span class="pull-right">Validez
                                <span class="sub">cliquez ici</span></span>
                                <i class="icon-ok"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /End Form -->

            <!--
                Loose Page
            =====================-->
            <div class="page loose endscreen">
                <div class="content">
                    <h1>Perdu ! <i class="icon-emo-unhappy"></i></h1>
                    <p> Retentez votre chance demain <br> <span class="hide invite-txt">ou invitez un ami pour rejouer</span></p>
                    <button type="submit" class="action-btn replay-btn hide">
                        <span class="pull-right">Rejouer
                        <span class="sub">cliquez ici</span></span>
                        <i class="icon-cw-1"></i>
                    </button>
                     <button type="submit" class="action-btn invite-btn hide">
                        <span class="pull-right">Inviter vos amis
                        <span class="sub">cliquez ici</span></span>
                        <i class="icon-user-plus"></i>
                    </button>
               </div>
            </div>
            <!-- /End Loose -->

            <!--
                Win Page
            =====================-->
            <div class="page win endscreen">
                <div class="content">
                    <h1>Félicitations ! <i class="icon-emo-happy"></i> </h1>
                    <p> <span>Votre participation au tirage au sort est enregistrée.</span> Vous remporterez peut être un <span>des 5 ensembles</span> offerts par Monoprix et London Dairy</p>
                    <img src="img/chaire.png" alt="Cadeaux">
               </div>
            </div>
            <!-- /End Win -->


            <div class="anim-pages">
                <div class="orange"></div>
                <div class="blue"></div>
                <div class="yellow"></div>
                <div class="white"></div>
            </div>



            {{--<div class="footer-wrapper">
                <a class="invite-link" href="#">Inviter des amis</a>
                •
                <a href="reglement.pdf" target="_blank">Réglement</a>
                •
                <a href="#" class="howto-btn">Comment jouer?</a>
            </div>--}}

        </div>

    </div>
    <center><img src="img/footer-app.jpg" alt=""></center>
    <!-- /app-wrapper -->
@stop
