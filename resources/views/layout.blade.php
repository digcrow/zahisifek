<!doctype html>
<!--[if lt IE 9]>      <html class="no-js lt-ie9" lang="{{ App::getLocale() }}"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="{{ App::getLocale() }}"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title', Config::get('app.name'))</title>
    <meta name="description" content="Participez au jeu Zahi Sifek et tentez de gagner un des 5 ensembles offerts par Monoprix et London Dairy">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <meta name="author" content="box.agency" /> -->

    <!-- Open Graph data -->
    <meta property="fb:app_id" content="{{ Config::get('services.facebook.client_id') }}"/>
    <meta property="og:image" content="@yield('meta.image')"/>
    <meta property="og:title" content="{{ Config::get('app.name') }}"/>
    <meta property="og:url" content="@yield('meta.url', url('/'))"/>
    <meta property="og:site_name" content="{{ Config::get('app.name') }}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:description" content="Participez au jeu Zahi Sifek et tentez de gagner un des 5 ensembles offerts par Monoprix et London Dairy"/>

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@site">
    <meta name="twitter:title" content="@yield('meta.title')">
    <meta name="twitter:description" content="@yield('meta.description')">
    <meta name="twitter:image" content="@yield('meta.image')">

    <!-- Metta CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="{{ asset('css/vendor.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">

    @yield('styles')

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="{{ asset('js/modernizr.js') }}"></script>
    <script>
        BASE_URL    = '{{ URL::to('/') }}';
        CURRENT_URL = '{{ URL::full() }}';
        FB_APP_ID   = '{{ Config::get('services.facebook.client_id') }}';

        // Redirect App from Canvas Page to Page Tab
        //redirectToPageTab = true;
        if ( document.referrer && document.referrer.indexOf("apps.facebook.com") === -1) {
            window.top.location.href = "{{ config('services.facebook.app_page') }}";
        }
    </script>
</head>
<body class="@yield('class')">

    <div class="loader">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <!--[if lt IE 9]>
    <div class="alert alert-dismissible outdated-browser show" role="alert">
        <h6>Votre navigateur est obsolète !</h6>
        <p>Pour afficher correctement ce site et bénéficier d'une expérience optimale, nous vous recommandons de mettre à jour votre navigateur.
            <a href="http://outdatedbrowser.com/fr" class="update-btn" target="_blank">Mettre à jour maintenant </a>
        </p>
        <a href="#" class="close-btn" title="Fermer" data-dismiss="alert" aria-label="Fermer">&times;</a>
    </div>
    <![endif]-->


    @yield('content')

    @yield('body')

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="{{ asset('js/jquery.min.js') }}"><\/script>')</script>

    <script src="{{ asset('js/vendor.js') }}"></script>
    <script src="{{ asset('js/TweenMax.min.js') }}"></script>
    <script src="{{ asset('js/progressbar.min.js') }}"></script>
    <script src="{{ asset('js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('js/jquery.widget.js') }}"></script>
    <script src="{{ asset('js/validatr.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>

    @yield('scripts')

   <script>
     (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
     (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
     m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
     })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

     ga('create', 'UA-65406684-1', 'auto');
     ga('send', 'pageview');
   </script>
</body>
</html>
