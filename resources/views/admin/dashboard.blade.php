<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin Panel - Zahi Sifek</title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
<body>
<div id="snippetContent" style="padding-top:10px;">
    <hr>
    <div class="container">
        <div class="alert alert-info" role="alert">
            Le nombre total des participants : <strong>{{ $count }}</strong> <br/>
            Le nombre des gagnants : <strong>{{ $countWinners }}</strong>
        </div>
        <div class="row">
            <div class="col-sm-10"><h1>Zahi Sifek</h1></div>
            {{--<div class="col-sm-2"><a href="/users" class="pull-right"><img title="profile image" class="img-circle img-responsive" src="http://www.gravatar.com/avatar/28fd20ccec6865e2d5f0e1f4446eb7bf?s=100"></a></div>--}}
        </div>
        <br/>
        <div class="row">
            <div class="col-sm-12">
                <table class="table">
                    <thead>
                        <th>#</th>
                        <th>Nom et prénom</th>
                        <th>Monoprix le plus proche</th>
                        <th>Num° LACARTE</th>
                        <th>CIN</th>
                        <th>Téléphone</th>
                        <th>Date de création</th>
                    </thead>
                    <tbody>
                    @foreach($participants as $key => $participant)
                        <tr @if($participant->winner) class="success" @endif>
                            <td>{{ ($participants->currentPage() - 1) * 10 + $key + 1}}</td>
                            <td><a href="https://www.facebook.com/{{ $participant->facebook_id }}" target="_blank">{{ $participant->name }}</a></td>
                            <td>{{ $participant->monoprix }}</td>
                            <td>{{ $participant->card }}</td>
                            <td>{{ $participant->phone }}</td>
                            <td>{{ $participant->cin }}</td>
                            <td>{{ $participant->created_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

        </div><!--/col-9-->
    </div><!--/row-->
        <div class="row">
            <div class="text-center">
                {!! $participants->appends(['username' => 'zahisifek'])->render() !!}
            </div>
        </div>
    <style type="text/css">
        body{margin-top:20px;}
    </style>

</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="{{ asset('js/jquery.min.js') }}"><\/script>')</script>

</body>
</html>