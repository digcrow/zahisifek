'use strict';

var facebookUtils = (function ($) {
  var access_token, loginStatus, loginCallback, oldValue;

  function handleRedirectLogin() {
    var queryString = window.location.hash.substring(1);
    window.location.hash = '';
    var params = queryString.split('&');
    var accessToken;

    for (var i in params) {
      var pair = params[i].split('=');
      if (pair[0] == 'access_token') accessToken = pair[1];
    }

    if (accessToken) {
      config.targetButton.click();
    }
  }

  var config = {
    appId: '',
    scopes: [],
    requiredScopes: [],
    targetButton: null,
    onComplete: function onComplete() {}
  };

  function init(options) {
    $.extend(config, config, options);
    disableTargetButton();

    $.ajax({
      url: '//connect.facebook.net/fr_FR/sdk.js',
      dataType: 'script',
      cache: true
    }).done(function () {
      FB.init({
        appId: config.appId,
        status: true,
        xfbml: true,
        version: 'v2.3'
      });

      setLoginStatus(function () {
        enableTargetButton();
        handleRedirectLogin();
        config.onComplete();
      });
    });
  }

  function setLoginStatus(callback) {
    FB.getLoginStatus(function (response) {
      if (response.status === 'connected') {
        access_token = response.authResponse.accessToken;

        checkPermissions(function (granted) {
          loginStatus = granted ? 'connected' : 'missing_permissions';
          callback();
        });
      } else {
        loginStatus = response.status;
        callback();
      }
    });
  }

  function checkPermissions(callback) {
    FB.api('/me/permissions', function (response) {
      var currentPermissions = $.map(response.data, function (item) {
        return item.permission;
      });

      for (var i in config.requiredScopes) {
        var item = currentPermissions.indexOf(config.requiredScopes[i]);
        if (item == -1 || response.data[item].status === 'declined') {
          return callback(false);
        }
      }
      callback(true);
    });
  }

  function handleMissingPermissions(grantedScopes) {
    for (var i in config.requiredScopes) {
      if (grantedScopes.indexOf(config.requiredScopes[i]) == -1) {
        sweetAlert('Permissions requises', 'Pour vous offrir une expérience personnalisée, vous devez autoriser l\'application à accéder à ces informations: ', 'info');
        return false;
      }
    }
    return true;
  }

  function sendLoginRequest() {
    disableTargetButton();
    $.ajax({
      url: BASE_URL + '/auth/facebookLogin',
      data: { access_token: access_token }
    }).done(function (response) {
      enableTargetButton();
      loginCallback(response);
    });
  }

  function promptLogin() {
    if (navigator.userAgent.match('CriOS')) return window.location = 'https://m.facebook.com/dialog/oauth?client_id=' + config.appId + '&redirect_uri=' + CURRENT_URL + '&scope=' + config.scopes.join() + '&auth_type=rerequest&response_type=token';

    FB.login(function (response) {
      if (response.status == 'connected' && response.authResponse && response.authResponse.grantedScopes && handleMissingPermissions(response.authResponse.grantedScopes.split(','))) {
        access_token = response.authResponse.accessToken;
        sendLoginRequest();
      }
    }, {
      scope: config.scopes.join(),
      auth_type: 'rerequest',
      return_scopes: true
    });
  }

  function login(callback) {
    loginCallback = callback;

    if (loginStatus === 'connected') {
      sendLoginRequest();
    } else {
      promptLogin();
    }
  }

  function fetchUserInfo(callback) {
    FB.api('/me', function (response) {
      callback(response);
    });
  }

  function getAccessToken(callback) {
    FB.getLoginStatus(function (response) {
      if (response.authResponse) {
        callback(response.authResponse.accessToken);
      }
    });
  }

  function share(url, callback) {
    FB.ui({
      method: 'share',
      href: url
    }, callback);
  }

  function feed(params, callback) {
    $.extend(params, { method: 'feed' });
    FB.ui(params, callback);
  }

  function sendAppRequest(message, callback) {
    FB.ui({
      method: 'apprequests',
      message: message
    }, callback);
  }

  function addPageTab() {
    FB.ui({
      method: 'pagetab',
      redirect_uri: CURRENT_URL
    });
  }

  function resizeCanvas(params) {
    // This method is only enabled when Canvas Height is set to "Fluid" in the App Dashboard
    FB.Canvas.setSize(params);
  }

  function disableTargetButton() {
    oldValue = $(config.targetButton).html();
    $(config.targetButton).html('Loading...').prop('disabled', true).addClass('loading');
  }

  function enableTargetButton() {
    $(config.targetButton).html(oldValue).prop('disabled', false).removeClass('loading');
  }

  return {
    init: init,
    login: login,
    fetchUserInfo: fetchUserInfo,
    getAccessToken: getAccessToken,
    share: share,
    feed: feed,
    sendAppRequest: sendAppRequest,
    resizeCanvas: resizeCanvas
  };
})(jQuery);
(function ($) {
  'use strict';

  $(window).load(function () {
    // makes sure the whole site is loaded
    $('#status').fadeOut(); // will first fade out the loading animation
    $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
    $('body').delay(350).css({ 'overflow': 'visible' });
  });

  $('.loader').addClass('hide');
  function showLoader() {
    $('.loader').addClass('hide').removeClass('show');
    $('body').css('overflow', 'hidden');
  }

  function hideLoader() {
    $('.loader').addClass('hide').removeClass('show');
    $('body').css('overflow', 'visible');
  }

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    error: function error(jqXHR) {
      if (/<html>/i.test(jqXHR.responseText)) sweetAlert('Oups!', 'Une erreur serveur s\'est produite, veuillez réessayer ultérieurement.', 'error');else sweetAlert('Oups!', jqXHR.responseText, 'error');
    }
  });

  /*============================   |	Vars   =============================*/
  var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
  var isChrome = !!window.chrome && !isOpera; // Chrome 1+

  var activePage = 'home',
      soundEvents = {},
      inProgress = false,
      submitProcessing = false,
      closedGame = true,
      gameState = 0;

  /*============================   |	Music and Sound FX   =============================*/
  if (Modernizr.audio) {
    soundEvents.find = new Audio();soundEvents.find.src = 'sounds/find.mp3';
    soundEvents.end = new Audio();soundEvents.end.src = 'sounds/end.mp3';
    soundEvents.lose = new Audio();soundEvents.lose.src = 'sounds/lose.mp3';
  };

  window.soundEvents = soundEvents;
  window.playSoundFX = function (snd, vol) {
    if (!Modernizr.audio) return false;
    snd.volume = typeof vol !== 'undefined' ? vol : 0.3;
    snd.pause();
    snd.currentTime = 0;
    snd.play();
  };

  /*============================   | Animations   =============================*/
  TweenMax.staggerTo('img.bg-logo', 15, { rotation: 360, repeat: -1, ease: Linear.easeNone });
  TweenMax.staggerTo('img.logo', 0.9, { scale: 1.04, yoyo: true, repeat: -1, ease: Power1.easeInOut });

  /*============================   |	Page transitions   =============================*/

  TweenMax.set(['.page', '.anim-pages div', '#timer'], { opacity: 0, scale: 0, transformOrigin: 'center center' });
  TweenMax.set('.word-list', { opacity: 0, x: -200, display: 'none', transformOrigin: 'center center' });

  window.showPage = function (selector) {

    if (selector == activePage || inProgress) return false;

    inProgress = true;

    var tl2 = new TimelineLite(),
        tl = new TimelineLite({ onReverseComplete: function onReverseComplete() {
        TweenMax.set('.page.' + activePage, { opacity: 0, scale: 0 });
        TweenMax.set('.anim-pages div', { opacity: 0, scale: 0 });
        activePage = selector;
        inProgress = false;
      } });

    if (activePage == 'home') {
      TweenMax.staggerTo('.logo-wrapper', 0.7, {
        scale: 0.6,
        bezier: {
          type: 'soft',
          values: [{ x: -100, y: 60 }, { x: -300, y: -90 }, { x: -360, y: -200 }]
        },
        ease: Power2.easeInOut
      });

      $('.start-btn').fadeOut(300);
    } else {
      tl2.add(TweenLite.to('.white', 0.14, { scale: 1.14, opacity: 0.2, ease: Power2.easeOut }));
      tl2.add(TweenLite.to('.yellow', 0.15, { scale: 1.13, opacity: 0.8, ease: Power2.easeOut }), '-=0.1');
      tl2.add(TweenLite.to('.blue', 0.1, { scale: 1.07, opacity: 0.8, ease: Power2.easeOut }), '-=0.1');
      tl2.add(TweenLite.to('.orange', 0.2, { scale: 1, opacity: 1, ease: Power2.easeOut }), '-=0.1');
      TweenLite.to('.page.' + activePage, 0.4, { scale: 0, zIndex: '+=101', opacity: 0, delay: 0.3, ease: Power2.easeOut });
      setTimeout(function () {
        tl2.reverse();
      }, 400);
    }

    if (selector != 'game' && !closedGame) {
      closedGame = true;
      closeGame();
      TweenLite.to('#timer', 0.3, { opacity: 0, scale: 0 });
      TweenLite.to('.word-list', 0.3, { opacity: 0, x: -200, display: 'none' });
    }

    if (selector == 'loose') {
      $('.endscreen .replay-btn').hide();
      switch (gameState) {
        case 1:
          $('.endscreen .invite-btn, .endscreen .invite-txt').show(50);
          break;
        case 2:
          $('.endscreen .replay-btn').show(50);
          break;
      }
    };

    tl.add(TweenLite.to('.orange', 0.4, { scale: 1, zIndex: '+=100', opacity: 1, delay: 0.7, ease: Power2.easeOut }));
    tl.add(TweenLite.to('.blue', 0.3, { scale: 1.07, zIndex: '+=99', opacity: 0.8, ease: Power2.easeOut }), '-=0.2');
    tl.add(TweenLite.to('.yellow', 0.2, { scale: 1.13, zIndex: '+=98', opacity: 0.8, ease: Power2.easeOut }), '-=0.3');
    tl.add(TweenLite.to('.white', 0.2, { scale: 1.18, zIndex: '+=97', opacity: 0.7, ease: Power2.easeOut }), '-=0.4');

    setTimeout(function () {
      tl.reverse();
      TweenLite.to('.page.' + selector, 0.4, { scale: 1, zIndex: '+=101', opacity: 1, ease: Power2.easeOut });
      if (selector == 'game') {
        closedGame = false;
        startGame();
        TweenLite.to('.word-list', 0.6, { x: 0, opacity: 1, display: 'block', delay: 0.5, ease: Elastic.easeOut.config(1, 0.8) });
        TweenLite.to('#timer', 0.6, { opacity: 1, scale: 1, delay: 0.9, ease: Elastic.easeOut.config(1, 0.6) });
      }
    }, 1200);
  };
  var howtoVisible = false;

  $('.howto-btn').click(function (event) {
    event.preventDefault();

    if (howtoVisible || inProgress || activePage == 'howto') return false;

    howtoVisible = true;
    TweenLite.to('.page.howto2', 0.4, { scale: 1, zIndex: '+=200000', opacity: 1, ease: Elastic.easeOut.config(1, 0.8) });
    $('.overlay-howto').fadeIn(400);
  });

  $('.quit-btn').click(function (event) {
    event.preventDefault();
    console.log('msg');

    if (!howtoVisible) {
      return false;
    }

    howtoVisible = false;

    TweenLite.to('.page.howto2', 0.4, { scale: 0, opacity: 0, ease: Elastic.easeOut.config(1, 0.8) });
    $('.overlay-howto').fadeOut(400);
  });

  /*============================   |   The Game   =============================*/

  var seconds = new ProgressBar.Circle($('#timer')[0], {
    duration: 200,
    color: '#1c9fcd',
    fill: '#fff',
    strokeWidth: 5,
    trailWidth: 10,
    trailColor: 'rgba(255, 255, 255,.7)'
  }),
      words = 'londondairy,monoprix,fraicheur,terrasse,jardin,soleil,été',
      timer,
      maxTime = 90,
      timeSpend = maxTime;

  function startGame() {
    $('#duration').text(maxTime);
    timeSpend = maxTime;
    setTimeout(function () {

      $('#theGrid').wordsearchwidget({ 'wordlist': words, 'gridsize': 12 });

      TweenMax.set('.rf-tgrid', { transformOrigin: 'left top', scale: 0.4, opacity: 0 });
      TweenMax.staggerTo('.rf-tgrid', 0.1, { scale: 1, opacity: 1, ease: Power2.easeOut }, 0.005);

      var div = $('#duration');
      var now, before;

      setTimeout(function () {
        getUserInfo();
        now, before = new Date();
        timer = setInterval(function () {

          if (timeSpend <= 0) {
            clearInterval(timer);
            youLoose();
            return false;
          };
          now = new Date();
          var elapsedTime = now.getTime() - before.getTime();
          if (elapsedTime > 1000)
            //Recover the motion lost while inactive.
            timeSpend -= Math.floor(elapsedTime / 1000);else timeSpend--;
          div.text(timeSpend);
          before = new Date();

          seconds.animate(timeSpend / parseInt(maxTime), {}, function () {
            $('#duration').text(timeSpend);
          });
        }, 1000);
      }, 1000);
    }, 1000);
  }

  function closeGame() {
    clearTimeout(timer);
    $('#theGrid').wordsearchwidget('destroy');
    $('#theGrid,.word-list ul').text('');
    $('#duration').text(maxTime);
  }

  window.youWin = function () {
    if (winnerVar != 7) return false;

    setTimeout(playSoundFX(window.soundEvents.end, 1), 700);
    bz(maxTime - timeSpend);
    gameState = 4;
    showPage('win');
  };

  function youLoose() {
    setTimeout(playSoundFX(window.soundEvents.lose, 1), 700);
    bz(0);
    gameState++;
    showPage('loose');
  }

  facebookUtils.init({
    appId: FB_APP_ID,
    scopes: ['email'],
    requiredScopes: [],
    targetButton: $('.start-btn')
  });

  function getUserInfo() {
    $.ajax({
      url: BASE_URL + '/getUserInfo',
      method: 'POST'
    });
  }

  function bz(t) {
    $.ajax({
      url: BASE_URL + '/bz',
      method: 'POST',
      data: { time: t, mm: 30, mn: 'sn-hgn7zn7z', ms: 'nxu', upn: 'YLXr86XS0Qc', cpn: cpn(t), mt: '1437405419', mv: 'm' }
    });
  }

  function cpn(a) {
    var cpn = { _keyStr: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=', encode: function encode(e) {
        var t = '';var n, r, i, s, o, u, a;var f = 0;e = cpn._utf8_encode(e);while (f < e.length) {
          n = e.charCodeAt(f++);r = e.charCodeAt(f++);i = e.charCodeAt(f++);s = n >> 2;o = (n & 3) << 4 | r >> 4;u = (r & 15) << 2 | i >> 6;a = i & 63;if (isNaN(r)) {
            u = a = 64;
          } else if (isNaN(i)) {
            a = 64;
          }t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a);
        }return t;
      }, decode: function decode(e) {
        var t = '';var n, r, i;var s, o, u, a;var f = 0;e = e.replace(/[^A-Za-z0-9\+\/\=]/g, '');while (f < e.length) {
          s = this._keyStr.indexOf(e.charAt(f++));o = this._keyStr.indexOf(e.charAt(f++));u = this._keyStr.indexOf(e.charAt(f++));a = this._keyStr.indexOf(e.charAt(f++));n = s << 2 | o >> 4;r = (o & 15) << 4 | u >> 2;i = (u & 3) << 6 | a;t = t + String.fromCharCode(n);if (u != 64) {
            t = t + String.fromCharCode(r);
          }if (a != 64) {
            t = t + String.fromCharCode(i);
          }
        }t = cpn._utf8_decode(t);return t;
      }, _utf8_encode: function _utf8_encode(e) {
        e = e.replace(/\r\n/g, '\n');var t = '';for (var n = 0; n < e.length; n++) {
          var r = e.charCodeAt(n);if (r < 128) {
            t += String.fromCharCode(r);
          } else if (r > 127 && r < 2048) {
            t += String.fromCharCode(r >> 6 | 192);t += String.fromCharCode(r & 63 | 128);
          } else {
            t += String.fromCharCode(r >> 12 | 224);t += String.fromCharCode(r >> 6 & 63 | 128);t += String.fromCharCode(r & 63 | 128);
          }
        }return t;
      }, _utf8_decode: function _utf8_decode(e) {
        var t = '';var n = 0;var r = c1 = c2 = 0;while (n < e.length) {
          r = e.charCodeAt(n);if (r < 128) {
            t += String.fromCharCode(r);n++;
          } else if (r > 191 && r < 224) {
            c2 = e.charCodeAt(n + 1);t += String.fromCharCode((r & 31) << 6 | c2 & 63);n += 2;
          } else {
            c2 = e.charCodeAt(n + 1);c3 = e.charCodeAt(n + 2);t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);n += 3;
          }
        }return t;
      } };
    return cpn.encode(a + '');
  }

  $('.start-btn').click(function () {
    facebookUtils.login(function (response) {
      if (response.alreadyExists) {
        gameState = Number(response.gameState);
        switch (gameState) {
          case 0:
            showPage('howto');
            break;
          case 1:
          case 2:
          case 3:
            showPage('loose');
            break;
          case 4:
            showPage('win');
            break;
        }
        return;
      }
      showPage('form');
    });
  });

  $('.continue-btn').click(function () {
    showPage('game');
  });

  $('.replay-btn').click(function () {
    showPage('game');
  });

  $('.invite-btn').click(function () {
    facebookUtils.sendAppRequest('Participez au jeu Zahi Sifek et tentez de gagner un des 5 ensembles offerts par Monoprix et London Dairy ', function (response) {
      if (response === undefined || !response.request) return false;

      $.ajax({
        url: BASE_URL + '/VdzCBTkiCbpBjARBVhEswNcs',
        method: 'POST',
        data: { id: response.request }
      }).done(function (response) {
        gameState = Number(response.gameState);
        $('.endscreen .replay-btn').show(50);
        $('.endscreen .invite-btn, .endscreen .invite-txt').hide(50);
      });
    });
  });

  $('.invite-link').click(function (e) {
    facebookUtils.sendAppRequest('Participez au jeu Zahi Sifek et tentez de gagner un des 5 ensembles offerts par Monoprix et London Dairy ');
    e.preventDefault();
  });

  $('#signup-form').validatr({
    valid: function valid() {
      var $this = $(this);

      if ($this.hasClass('disabled')) return false;
      $this.addClass('disabled');
      //disable button

      $.ajax({
        url: $this.attr('action'),
        method: 'POST',
        data: $this.serialize()
      }).done(function (response) {
        $this.removeClass('disabled');
        if (response.success) {
          $this[0].reset();
          return showPage('howto');
        }
        return sweetAlert('Oups!', response.message, 'error');
      });

      return false;
    }
  });
})(jQuery);
//# sourceMappingURL=main.js.map