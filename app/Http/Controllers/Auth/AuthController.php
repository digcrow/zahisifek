<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;

use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphUser;
use Facebook\FacebookRequestException;
use Config;
use App\Participant;
use Carbon\Carbon;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    public function facebookLogin(Request $request)
    {
        FacebookSession::setDefaultApplication(
            Config::get('services.facebook.client_id'),
            Config::get('services.facebook.client_secret')
        );

        $session = new FacebookSession($request->input('access_token'));

        try {
            $session->validate();
        } catch(FacebookRequestException $ex) {
            return response('Exception occured, code: ' . $ex->getCode() . ' with message: ' . $ex->getMessage(), 500);
        } catch(\Exception $ex) {
            return response('Exception occured, code: ' . $ex->getCode() . ' with message: ' . $ex->getMessage(), 500);
        }

        if ($session) {
            try {
                $me = (new FacebookRequest(
                    $session, 'GET', '/me?fields=name,gender,email'
                ))->execute()->getGraphObject( GraphUser::className() );

                $participant = Participant::where('facebook_id', $me->getId())->first();

                if ( $participant ) {
                    session(['participantId' => $participant->id]);

                    $today = Carbon::today()->day;
                    $lastPlayed = $participant->updated_at->day;

                    if ( $today - $lastPlayed >= 1 && $participant->game_state != 0 && !$participant->winner ) {
                        $participant->game_state = 0;
                        $participant->save();
                    }

                    return response()->json(['alreadyExists' => true, 'gameState' => $participant->winner ? 4 : $participant->game_state]);
                }

                $participant = new Participant;
                $participant->name        = $me->getName();
                $participant->facebook_id = $me->getId();
                $participant->email       = $me->getEmail();
                $participant->gender      = $me->getProperty('gender');

                session(['participant' => $participant]);
                return response()->json(['alreadyExists' => false]);

            } catch(FacebookRequestException $ex) {
                return response('Exception occured, code: ' . $ex->getCode() . ' with message: ' . $ex->getMessage(), 500);
            }
        }

        return response('Une erreur inconnue s\'est produite. Veuillez réessayer.', 500);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
