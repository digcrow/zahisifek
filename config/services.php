<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => '',
        'secret' => '',
    ],

	'mandrill' => [
		'secret' => 'tXI5A0bhNM2o-N2VHdDMoQ',
	],

    'ses' => [
        'key'    => '',
        'secret' => '',
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model'  => App\User::class,
        'key'    => '',
        'secret' => '',
    ],

	/*
    |--------------------------------------------------------------------------
    | Facebook App Config
    |--------------------------------------------------------------------------
    */

	'facebook' => [
		'client_id'     => '1426660094328889',
		'client_secret' => 'f4d2f4a0114472ccf5971df8b8047c37',
		'redirect'      => env('APP_URL') ,
		'app_page'      => 'https://apps.facebook.com/zahisifek',
	],

];
